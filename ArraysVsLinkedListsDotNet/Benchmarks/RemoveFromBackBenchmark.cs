using Benchy;
using Estruturas;

namespace Benchmarks;

public class RemoveFromBackBenchmark<TList> : IBenchmark where TList : IMyList<int>, new()
{
    private TList list;
    private int numOfIterations;

    public RemoveFromBackBenchmark(int numOfIterations)
    {
        this.list = default!;
        this.numOfIterations = numOfIterations;
    }

    public void Before()
    {
        list = new();
        for (int i = 0; i < numOfIterations; i++)
        {
            list.FastInsert(i);
        }
    }

    public void Run()
    {
        for (int i = 0; i < numOfIterations; i++)
        {
            list.RemoveFromBack();
        }
    }

    public void After()
    {
    }
}