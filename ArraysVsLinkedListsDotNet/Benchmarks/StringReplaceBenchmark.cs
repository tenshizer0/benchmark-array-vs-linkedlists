using BenchmarkDotNet.Attributes;
using Benchy;

namespace Benchmarks;

[MemoryDiagnoser]
public class StringReplaceBenchmark : IBenchmark
{
    public void Before()
    {
    }

    [Benchmark]
    public void Run()
    {
        var s1 = "text with\nnew line";
        var s2 = s1.Replace("\r", "").Replace("\n", "");
    }

    public void After()
    {
    }
}