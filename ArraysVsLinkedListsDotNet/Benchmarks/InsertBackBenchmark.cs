using Benchy;
using Estruturas;

namespace Benchmarks;

public class InsertBackBenchmark<TList> : IBenchmark where TList : IMyList<int>, new()
{
    private TList list;
    private int numOfIterations;

    public InsertBackBenchmark(int numOfIterations)
    {
        this.list = default!;
        this.numOfIterations = numOfIterations;
    }

    public void Before()
    {
        list = new();
    }

    public void Run()
    {
        for (int i = 0; i < numOfIterations; i++)
        {
            list.InsertBack(i);
        }
    }

    public void After()
    {
    }
}