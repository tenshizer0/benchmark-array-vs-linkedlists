using BenchmarkDotNet.Attributes;
using Benchy;
using Estruturas;

namespace Benchmarks;

[MemoryDiagnoser]
public class ReverseBenchmark
{
    private MyArrayList<int> myArrayList = new();
    private MyLinkedList<int> myLinkedList = new();
    private ListWrapper<int> list = new();
    private LinkedListWrapper<int> linkedList = new();

    [GlobalSetup]
    public void Setup()
    {
        for (int i = 0; i < 200_000; i++)
        {
            myArrayList.FastInsert(i);
            myLinkedList.FastInsert(i);
            list.FastInsert(i);
            linkedList.FastInsert(i);
        }
    }

    [Benchmark]
    public void MyArrayListReverse()
    {
        myArrayList.Reverse();
    }

    [Benchmark]
    public void MyLinkedListReverse()
    {
        myLinkedList.Reverse();
    }

    [Benchmark]
    public void ListReverse()
    {
        list.Reverse();
    }

    [Benchmark]
    public void LinkedListReverse()
    {
        linkedList.Reverse();
    }
}