﻿using BenchmarkDotNet.Running;
using Benchmarks;
using Benchy;
using Estruturas;

const int numOfInsertions = 200_000;
const int numOfIterations = 10;

Runner runner;

// Console.WriteLine("MyArrayList.InsertInFront");
// var myArrayInsertInFront = new InsertInFrontBenchmark<MyArrayList<int>>(numOfInsertions);
// runner = new Runner(myArrayInsertInFront, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("MyLinkedList.InsertInFront");
// var myLinkedListInsertInFront = new InsertInFrontBenchmark<MyLinkedList<int>>(numOfInsertions);
// runner = new Runner(myLinkedListInsertInFront, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("List.InsertInFront");
// var listInsertInFront = new InsertInFrontBenchmark<ListWrapper<int>>(numOfInsertions);
// runner = new Runner(listInsertInFront, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("LinkedList.InsertInFront");
// var linkedListInsertInFront = new InsertInFrontBenchmark<LinkedListWrapper<int>>(numOfInsertions);
// runner = new Runner(linkedListInsertInFront, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("MyArrayList.InsertBack");
// var myArrayInsertBack = new InsertBackBenchmark<MyArrayList<int>>(numOfInsertions);
// runner = new Runner(myArrayInsertBack, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("MyLinkedList.InsertBack");
// var myLinkedListInsertBack = new InsertBackBenchmark<MyLinkedList<int>>(numOfInsertions);
// runner = new Runner(myLinkedListInsertBack, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("List.InsertBack");
// var listInsertBack = new InsertBackBenchmark<ListWrapper<int>>(numOfInsertions);
// runner = new Runner(listInsertBack, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("LinkedList.InsertBack");
// var linkedListInsertBack = new InsertBackBenchmark<LinkedListWrapper<int>>(numOfInsertions);
// runner = new Runner(linkedListInsertBack, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("MyArrayList.RemoveFromFront");
// var myArrayRemoveFromFront = new RemoveFromFrontBenchmark<MyArrayList<int>>(numOfInsertions);
// runner = new Runner(myArrayRemoveFromFront, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("MyLinkedList.RemoveFromFront");
// var myLinkedListRemoveFromFront = new RemoveFromFrontBenchmark<MyLinkedList<int>>(numOfInsertions);
// runner = new Runner(myLinkedListRemoveFromFront, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("List.RemoveFromFront");
// var listRemoveFromFront = new RemoveFromFrontBenchmark<ListWrapper<int>>(numOfInsertions);
// runner = new Runner(listRemoveFromFront, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("LinkedList.RemoveFromFront");
// var linkedListRemoveFromFront = new RemoveFromFrontBenchmark<LinkedListWrapper<int>>(numOfInsertions);
// runner = new Runner(linkedListRemoveFromFront, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("MyArrayList.RemoveFromBack");
// var myArrayRemoveFromBack = new RemoveFromBackBenchmark<MyArrayList<int>>(numOfInsertions);
// runner = new Runner(myArrayRemoveFromBack, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("MyLinkedList.RemoveFromBack");
// var myLinkedListRemoveFromBack = new RemoveFromBackBenchmark<MyLinkedList<int>>(numOfInsertions);
// runner = new Runner(myLinkedListRemoveFromBack, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("List.RemoveFromBack");
// var listRemoveFromBack = new RemoveFromBackBenchmark<ListWrapper<int>>(numOfInsertions);
// runner = new Runner(listRemoveFromBack, numOfIterations);
// runner.Run();

// Console.WriteLine();

// Console.WriteLine("LinkedList.RemoveFromBack");
// var linkedListRemoveFromBack = new RemoveFromBackBenchmark<LinkedListWrapper<int>>(numOfInsertions);
// runner = new Runner(linkedListRemoveFromBack, numOfIterations);
// runner.Run();

// Console.WriteLine();

// BenchmarkRunner.Run<ReverseBenchmark>();