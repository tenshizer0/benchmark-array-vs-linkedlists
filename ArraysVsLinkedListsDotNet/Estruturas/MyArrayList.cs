namespace Estruturas;

public class MyArrayList<T> : IMyList<T>
{
    public T[] Items { get; private set; }
    public int Length { get; private set; }
    public int Capacity { get; private set; }

    public MyArrayList()
    {
        Capacity = 16;
        Items = new T[Capacity];
        Length = 0;
    }

    public MyArrayList(int capacity)
    {
        Items = new T[capacity];
        Length = 0;
        Capacity = capacity;
    }

    public T? First()
    {
        if (Length == 0)
            return default;
        return Items[0];
    }

    public T? Last()
    {
        if (Length == 0)
            return default;
        return Items[Length - 1];
    }

    public T At(int position)
    {
        return Items[position];
    }

    public void InsertInFront(T element)
    {
        T[] from = Items;
        T[] to;

        if (Length < Capacity)
        {
            to = Items;
        }
        else
        {
            Capacity *= 2;
            to = new T[Capacity];
            Items = to;
        }

        if (Length % 4 == 0)
        {
            for (int i = Length - 4; i > 0; i -= 4)
            {
                to[i] = from[i - 4];
                to[i + 1] = from[i - 3];
                to[i + 2] = from[i - 2];
                to[i + 3] = from[i - 1];
            }
        }
        for (int i = Length - 1; i > 0; i--)
        {
            to[i] = from[i - 1];
        }

        to[0] = element;
        Length++;
    }

    public void InsertBack(T element)
    {
        if (Length >= Capacity)
        {
            Capacity *= 2;
            T[] tmp = new T[Capacity];
            for (int i = 0; i < Length; i++)
            {
                tmp[i] = Items[i];
            }
            Items = tmp;
        }
        Items[Length] = element;
        Length++;
    }

    public void InsertAt(int position, T element)
    {
        if (position > Length)
            throw new IndexOutOfRangeException();

        T[] from = Items;
        T[] to;

        if (Length <= Capacity)
        {
            to = Items;
        }
        else
        {
            Capacity *= 2;
            to = new T[Capacity];
            for (int i = 0; i < position; i++)
            {
                to[i] = Items[i];
            }
        }

        for (int i = Length - 1; i > position; i--)
        {
            to[i] = from[i - 1];
        }

        to[position] = element;
        Items = to;
        Length++;
    }

    public void RemoveFromFront()
    {
        if (Length == 0)
            throw new InvalidOperationException();

        Length--;
        for (int i = 0; i < Length; i++)
        {
            Items[i] = Items[i + 1];
        }
    }

    public void RemoveFromBack()
    {
        if (Length == 0)
            throw new InvalidOperationException();

        Length--;
    }

    public void RemoveFrom(int position)
    {
        if (Length == 0)
            throw new InvalidOperationException();

        Length--;
        for (int i = position; i < Length; i++)
        {
            Items[i] = Items[i + 1];
        }
    }

    public void Reverse()
    {
        int middle = Length / 2;
        for (int i = 0; i < middle; i++)
        {
            int j = Length - i - 1;
            T tmp = Items[i];
            Items[i] = Items[j];
            Items[j] = tmp;
        }
    }

    public void Concatenate(IMyList<T> other)
    {
        var totalLength = Length + other.Length;
        if (totalLength > Capacity)
        {
            var exponent = (int)Math.Log2(totalLength) + 1;
            Capacity = (int)Math.Pow(2, exponent);
            var tmp = new T[Capacity];
            for (int i = 0; i < Length; i++)
            {
                tmp[i] = Items[i];
            }
            Items = tmp;
        }

        for (int i = 0; i < other.Length; i++)
        {
            Items[Length + i] = other.At(i);
        }

        Length = totalLength;
        other.Clear();
    }

    public void Clear()
    {
        Length = 0;
    }

    public void Print(bool printElements = true)
    {
        Console.Write("Length: {0}, ", Length);
        Console.Write("Capacity: {0}", Capacity);

        if (!printElements)
            return;

        Console.Write(", [ ");
        for (int i = 0; i < Length; i++)
        {
            Console.Write($"{Items[i]} ");
        }
        Console.WriteLine("]");
    }

    public void FastInsert(T value)
    {
        InsertBack(value);
    }
}