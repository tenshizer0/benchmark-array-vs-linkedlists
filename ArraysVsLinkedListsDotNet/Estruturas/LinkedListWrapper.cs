namespace Estruturas;

public class LinkedListWrapper<T> : IMyList<T>
{
    private LinkedList<T> list = new();

    public int Length => list.Count;

    public int Capacity => list.Count;

    public T At(int position)
    {
        return list.ElementAt(position);
    }

    public void Clear()
    {
        list.Clear();
    }

    public void Concatenate(IMyList<T> other)
    {
        throw new NotImplementedException();
    }

    public void FastInsert(T value)
    {
        InsertInFront(value);
    }

    public T? First()
    {
        return list.FirstOrDefault();
    }

    public void InsertAt(int position, T value)
    {
        throw new NotImplementedException();
    }

    public void InsertBack(T value)
    {
        var node = new LinkedListNode<T>(value);
        list.AddLast(node);
    }

    public void InsertInFront(T value)
    {
        var node = new LinkedListNode<T>(value);
        list.AddFirst(node);
    }

    public T? Last()
    {
        return list.LastOrDefault();
    }

    public void RemoveFrom(int position)
    {
        throw new NotImplementedException();
    }

    public void RemoveFromBack()
    {
        list.RemoveLast();
    }

    public void RemoveFromFront()
    {
        list.RemoveFirst();
    }

    public void Reverse()
    {
        var newList = new LinkedList<T>();
        foreach (var item in list)
        {
            newList.AddFirst(item);
        }
        list = newList;
    }
}