namespace Estruturas;

public class MyLinkedList<T> : IMyList<T>
{
    public MyLinkedListNode<T>? Header { get; private set; }
    public int Length { get; private set; }
    public int Capacity => Length;

    public MyLinkedList()
    {
        Header = null;
        Length = 0;
    }

    public T? First()
    {
        if (Header is null)
            return default;
        return Header.Value;
    }

    public T? Last()
    {
        if (Header is null)
            return default;
        var last = Header;
        while (last.Next is not null)
            last = last.Next;
        return last.Value;
    }

    public T At(int position)
    {
        if (position >= Length)
            throw new IndexOutOfRangeException();

        var item = Header;
        for (int i = 0; i < position; i++)
        {
            item = item?.Next;
            if (item is null)
                throw new InvalidOperationException();
        }
        return item!.Value;
    }

    public void InsertInFront(T element)
    {
        var item = new MyLinkedListNode<T>(element, Header);
        Header = item;
        Length++;
    }

    public void InsertBack(T value)
    {
        if (Header is null)
        {
            InsertInFront(value);
            return;
        }

        var last = Header;
        while (last.Next is not null)
            last = last.Next;

        var item = new MyLinkedListNode<T>(value);
        last.Next = item;
        Length++;
    }

    public void InsertAt(int position, T value)
    {
        if (Header is null || position == 0)
        {
            InsertInFront(value);
            return;
        }

        if (position == Length)
        {
            InsertBack(value);
            return;
        }

        if (position > Length)
            throw new IndexOutOfRangeException();

        var prev = Header;
        for (int i = 0; i < position - 1; i++)
        {
            prev = prev.Next;
            if (prev is null)
                throw new IndexOutOfRangeException();
        }
        var item = new MyLinkedListNode<T>(value, prev.Next);
        prev.Next = item;
        Length++;
    }

    public void RemoveFromFront()
    {
        if (Header is null)
            throw new InvalidOperationException();

        Header = Header.Next;
        Length--;
    }

    public void RemoveFromBack()
    {
        if (Header is null)
            throw new InvalidOperationException();

        if (Header.Next is null)
        {
            Header = null;
            return;
        }

        var prev = Header;
        while (prev.Next?.Next is not null)
            prev = prev.Next;

        prev.Next = null;
        Length--;
    }

    public void RemoveFrom(int position)
    {
        if (Header is null)
            throw new InvalidOperationException();

        if (position == 0)
        {
            RemoveFromFront();
            return;
        }

        if (position >= Length)
            throw new IndexOutOfRangeException();

        Length--;
    }


    public void Reverse()
    {
        var list = Header;
        MyLinkedListNode<T>? newList = null;
        while (list is not null)
        {
            var tmp = list;
            list = list.Next;
            tmp.Next = newList;
            newList = tmp;
        }
        Header = newList;
    }

    public void Concatenate(IMyList<T> other)
    {
        var list = other as MyLinkedList<T>;
        if (list is not null)
        {
            ConcatenateLinkedList(list);
            return;
        }

        Reverse();
        for (int i = 0; i < other.Length; i++)
        {
            InsertInFront(other.At(i));
        }
        Reverse();
        other.Clear();
    }

    private void ConcatenateLinkedList(MyLinkedList<T> list)
    {
        if (Header is null)
        {
            Header = list.Header;
            Length = list.Length;
            list.Clear();
            return;
        }

        var last = Header;
        while (last.Next is not null)
            last = last.Next;

        last.Next = list.Header;
        Length += list.Length;
        list.Clear();
    }

    public void Clear()
    {
        Header = null;
        Length = 0;
    }

    public void Print(bool printElements = true)
    {
        Console.Write("Length: {0}, ", Length);
        Console.Write("Capacity: {0}", Capacity);

        if (!printElements)
            return;

        Console.Write(", [ ");
        for (var item = Header; item is not null; item = item.Next)
        {
            Console.Write($"{item.Value} ");
        }
        Console.WriteLine("]");
    }

    public void FastInsert(T value)
    {
        InsertInFront(value);
    }
}

public class MyLinkedListNode<T>
{
    public T Value { get; set; }
    public MyLinkedListNode<T>? Next { get; set; }

    public MyLinkedListNode(T value, MyLinkedListNode<T>? next = null)
    {
        Value = value;
        Next = next;
    }
}