namespace Estruturas;

public class ListWrapper<T> : IMyList<T>
{
    private List<T> Items = new();

    public int Length => Items.Count;

    public int Capacity => Items.Capacity;

    public T At(int position)
    {
        return Items[position];
    }

    public void Clear()
    {
        Items.Clear();
    }

    public void Concatenate(IMyList<T> other)
    {
        throw new NotImplementedException();
    }

    public void FastInsert(T value)
    {
        InsertInFront(value);
    }

    public T? First()
    {
        return Items.FirstOrDefault();
    }

    public void InsertAt(int position, T value)
    {
        Items.Insert(position, value);
    }

    public void InsertBack(T value)
    {
        Items.Add(value);
    }

    public void InsertInFront(T value)
    {
        Items.Insert(0, value);
    }

    public T? Last()
    {
        return Items.LastOrDefault();
    }

    public void RemoveFrom(int position)
    {
        Items.RemoveAt(position);
    }

    public void RemoveFromBack()
    {
        Items.RemoveAt(Length - 1);
    }

    public void RemoveFromFront()
    {
        Items.RemoveAt(0);
    }

    public void Reverse()
    {
        Items.Reverse();
    }
}