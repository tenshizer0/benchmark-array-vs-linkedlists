namespace Estruturas;

public interface IMyList<T>
{
    int Length { get; }
    int Capacity { get; }
    T? First();
    T? Last();
    T At(int position);
    void FastInsert(T value);
    void InsertInFront(T value);
    void InsertBack(T value);
    void InsertAt(int position, T value);
    void RemoveFromFront();
    void RemoveFromBack();
    void RemoveFrom(int position);
    void Reverse();
    void Concatenate(IMyList<T> other);
    void Clear();
}