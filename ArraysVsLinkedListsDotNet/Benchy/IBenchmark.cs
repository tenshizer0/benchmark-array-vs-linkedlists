namespace Benchy;

public interface IBenchmark
{
    void Before();
    void Run();
    void After();
}