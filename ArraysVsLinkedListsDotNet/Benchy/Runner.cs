using System.Diagnostics;

namespace Benchy;

public class Runner
{
    private IBenchmark benchmark;
    private Stopwatch timer;
    private double[] elapsedTimes;
    private long memoryUsage;
    private int[] gcCollectionCounts;
    private long numOfIterations;

    public Runner(IBenchmark benchmark, long numOfIterations = 10)
    {
        this.benchmark = benchmark;
        timer = new();
        elapsedTimes = new double[numOfIterations];
        gcCollectionCounts = new int[GC.MaxGeneration + 1];
        this.numOfIterations = numOfIterations;
    }

    public void Run()
    {
        RunWarmUp();

        RunBenchmarck();

        // estatísticas
        var meanTime = elapsedTimes.ToList().Sum() / numOfIterations;
        var stdDevSum = elapsedTimes
            .ToList()
            .Select(elapsedTime => elapsedTime - meanTime)
            .Select(val => val * val)
            .Sum();
        var stdDev = Math.Sqrt(stdDevSum / elapsedTimes.Length);
        Console.WriteLine("mean time:   {0} ms", meanTime);
        Console.WriteLine("stddev time: {0} ms", stdDev);
        for (int i = 0; i < gcCollectionCounts.Length; i++)
        {
            Console.WriteLine("GC Gen {0}:    {1} per iteration", i, (decimal)gcCollectionCounts[i] / numOfIterations);
        }
        Console.WriteLine("memory:      {0} B", memoryUsage / numOfIterations);
    }

    private void RunWarmUp()
    {
        Console.WriteLine("Start WarmUp");
        RunIterations(numOfIterations / 2);
        Console.WriteLine("End WarmUp");
    }

    private void RunBenchmarck()
    {
        Console.WriteLine("Start Benchmark");
        ClearData();
        RunIterations(numOfIterations);
        Console.WriteLine("End Benchmark");
    }

    private void ClearData()
    {
        timer.Reset();
        for (int i = 0; i < elapsedTimes.Length; i++)
        {
            elapsedTimes[i] = 0;
        }
        memoryUsage = 0;
        for (int i = 0; i < gcCollectionCounts.Length; i++)
        {
            gcCollectionCounts[i] = 0;
        }
        GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);
    }

    private void RunIterations(long numOfIterations)
    {
        for (int i = 0; i < numOfIterations; i++)
        {
            Console.Write("Iteration {0}... ", i);
            benchmark.Before();

            CountGcCollections();

            var memoryStart = GC.GetTotalAllocatedBytes(true);

            timer.Start();

            benchmark.Run();

            timer.Stop();

            var memoryEnd = GC.GetTotalAllocatedBytes(true);

            CountGcCollections();

            memoryUsage += memoryEnd - memoryStart;

            elapsedTimes[i] = timer.Elapsed.TotalMilliseconds;

            timer.Reset();

            benchmark.After();
            Console.WriteLine("{0} ms", elapsedTimes[i]);
        }
    }

    private void CountGcCollections()
    {
        for (int i = 0; i < gcCollectionCounts.Length; i++)
        {
            gcCollectionCounts[i] = GC.CollectionCount(i) - gcCollectionCounts[i];
        }
    }
}